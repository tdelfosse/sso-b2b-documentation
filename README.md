# Exemple d'utilisation de l'attribut contact
## Version non formatée
```js
contactsAsObject = {
    1: {
        firstname: "Thomas",
        lastname: "Loiseau",
        phone: "0561880199",
        email: "thomas.loiseau@atos.net",
        job: "Developpeur"
    },
    2: {
        firstname: "Martin",
        lastname: "Michel",
        phone: "0925369785",
        email: "martin.michel@atos.net",
        job: "Directeur"
    },
    3: {
        firstname: "Jerome",
        lastname: "Dupont",
        phone: "0598547523",
        email: "jerome.dupont@atos.net",
        job: "Responsable projet"
    },
    3: {
        firstname: "Antoine",
        lastname: "Jacquemond",
        phone: "0752694521",
        email: "antoine.jacquemond@atos.net",
        job: "Developpeur"
    },
    4: {
        firstname: "Jean",
        lastname: "Petit",
        phone: "0659736406",
        email: "jean.petit@atos.net",
        job: "Responsable commercial"
    },
    5: {
        firstname: "Pierre",
        lastname: "Durant",
        phone: "0489364885",
        email: "pierre.durant@atos.net",
        job: "Developpeur"
    }
}
```

## Version formatée
### Attribut contact1 :
```
Thomas|Loiseau|0561880199|thomas.loiseau@atos.net|Developpeur;Martin|Michel|0925369785|martin.michel@atos.net|Directeur;Antoine|Jacquemond|0752694521|antoine.jacquemond@atos.net|Developpeur;Jean|Petit|0659736406|jean.petit@atos.net|Responsable commercial;
```
### Attribut contact2 :
```
Pierre|Durant|0489364885|pierre.durant@atos.net|Developpeur;
```
### Attribut contact3 :
```

```
### Attribut contact4 :
```

```

## Explication
Keyckloack, qui est la solution choisie pour déployer le SSO partenaire, permet de créer des attributs que nous utilisons pour stocker les contacts. Cependant, il existe une contrainte technique de taille, ces attributs ne peuvent stocker que des strings dont la taille ne peut excéder 255 caractères. 

Un contact étant un objet contenant diverses informations (nom, prénom, tél, email, job), j'ai dû trouver une manière de concaténer ces informations. 

### Format :
![format](format.png)
Chaque information est séparé par un PIPE ( | ) et chaque contact est séparé par un POINT VIRGULE ( ; ). L'ordre dans lequel sont saisies les informations doit être respecté.

## Les fonctions :
### Pour l'envoi :

```js
/**
* Formate les données (objet) en un objet de 4 chaines de caractères.
* Ces 4 chaines correspondent aux 4 attributs nommés "contactN" d'un utilisateur (N étant un chiffre allant de 1 à 4).
* Chaque chaine ne peut excéder 255 caractères.
* @param {object} data 
*/
function formatData(data) {
    let contacts = {
        1: "",
        2: "",
        3: "",
        4: ""
    }

    // On parcours l'objet < data > contenant nos contacts
    for (const i in data) {
        // On formate notre contact en une chaine de caractères
        let contactAsString = createContactAsString(data[i]);
        // On parcours l'objet < contacts > pour la concaténation
        for (const j in contacts) {
            // Si l'attribut peut contenir les informations du contact alors on concatène
            // Sinon on passe à l'attribut suivant
            if (!isBiggerThanAuthorize(contacts[j], contactAsString)) {
                contacts[j] += contactAsString;
                break;
            }
        }
    }
    return contacts;
}

/**
* Concatène toutes les informations d'un contact en une chaine de caractères formatée.
* @param {object} contact 
*/
function createContactAsString(contact) {
    return `${contact.firstname}|${contact.lastname}|${contact.phone}|${contact.email}|${contact.job};`
}

/**
* Vérifie si la concaténation d'un attribut avec l'ajout contact ne dépasse pas les 255 caractères.
* @param {string} oldString 
* @param {string} newEntry 
*/
function isBiggerThanAuthorize(oldString, newEntry) {
    let newString = oldString + newEntry;
    return newString.length > 255 ? true : false;
}
```

### Pour la réception :

```js
/**
* Fonction qui va parser les données brutes pour créer un tableau de contacts
* Le paramètre < data > est la concaténations des 4 attributs "contactN" de l'utilisateur.
* @param {string} data 
*/
function parseData(data) {
    return data.split(";");
}

/**
* Fonction qui va parser les contacts depuis une chaine de caractères
* @param {string} data 
*/
function parseContact(data) {
    var contacts = [];
    for (item in data) {
            contacts[item] = formateContact(data[item].split("|"));
    }
    return contacts;
}

/**
*  Fonction qui va formater les valeurs d'un contact
* @param {string} contact 
*/
function formateContact(contact) {
    var object = {
            firstname: contact[0],
            lastname: contact[1],
            phone: contact[2],
            email: contact[3],
            job: contact[4]
    }
    return object;
}
```