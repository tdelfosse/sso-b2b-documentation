contactsAsObject = {
    1: {
        firstname: "Thomas",
        lastname: "Loiseau",
        phone: "0561880199",
        email: "thomas.loiseau@atos.net",
        job: "Developpeur"
    },
    2: {
        firstname: "Martin",
        lastname: "Michel",
        phone: "0925369785",
        email: "martin.michel@atos.net",
        job: "Directeur"
    },
    3: {
        firstname: "Jerome",
        lastname: "Dupont",
        phone: "0598547523",
        email: "jerome.dupont@atos.net",
        job: "Responsable projet"
    },
    3: {
        firstname: "Antoine",
        lastname: "Jacquemond",
        phone: "0752694521",
        email: "antoine.jacquemond@atos.net",
        job: "Developpeur"
    },
    4: {
        firstname: "Jean",
        lastname: "Petit",
        phone: "0659736406",
        email: "jean.petit@atos.net",
        job: "Responsable commercial"
    },
    5: {
        firstname: "Pierre",
        lastname: "Durant",
        phone: "0489364885",
        email: "pierre.durant@atos.net",
        job: "Developpeur"
    }
}

/**
 * Concat every informations of a contact into a formated string
 * @param {object} contact 
 */
function createContactAsString(contact) {
    return `${contact.firstname}|${contact.lastname}|${contact.phone}|${contact.email}|${contact.job};`
}

/**
 * Check if the concat of 2 strings length is bigger than 255 characters 
 * @param {string} oldString 
 * @param {string} newEntry 
 */
function isBiggerThanAuthorize(oldString, newEntry) {
    let newString = oldString + newEntry;
    return newString.length > 255 ? true : false;
}

/**
 * Format data recieved as object into an object of 4 strings.
 * These 4 strings match with the 4 contacts attributes that need to be send when create or update user.
 * Each attribute can only store a string of maximum 255 characters.
 * @param {object} data 
 */
function formatData(data) {
    let contacts = {
        1: "",
        2: "",
        3: "",
        4: ""
    }

    for (const i in data) {
        let contactAsString = createContactAsString(data[i]);
        for (const j in contacts) {
            if (!isBiggerThanAuthorize(contacts[j], contactAsString)) {
                contacts[j] += contactAsString;
                break;
            }
        }
    }
    return contacts;
}


/*
 * Next functions are not important 
*/
function addUnformatedVersion() {
    let cards = document.getElementById("cards");
    for (const i in contactsAsObject) {
        cards.append(createCard(contactsAsObject[i]));
    }
}

function createCard(contact) {
    let card = document.createElement("div");
    card.setAttribute("class", "card");
    for (const i in contact) {
        let element = document.createElement("div");
        element.setAttribute("class", i);
        element.append(contact[i]);
        card.append(element);
    }
    return card;
}

addUnformatedVersion();

// Transform and store the contacts
let contactsFormated = formatData(contactsAsObject);

// Insert ine the right input to display the new format
for (const i in contactsFormated) {
    document.getElementById(`contact${i}`).value = contactsFormated[i];
}